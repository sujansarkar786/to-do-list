<?php
require_once "vendor/autoload.php";

use App\Db;

$message = '';
if (isset($_POST['submit']) && !empty($_POST['title'])) {
    $message = Db::saveData($_POST);
}
$getData = Db::getData();
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $getDelete = Db::delete($id);
}



?>
<!DOCTYPE html>
<html>

<head>
    <title>TodoMVC</title>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/todomvc-app-css@2.2.0/index.css" />
</head>

<body>
    <section class="todoapp">
        <header class="header">
            <h1>todos</h1>
            <h3><?php echo $message; ?></h3>
            <form action="" method="post">
                <input class="new-todo" id="title" name="title" autofocus autocomplete="off" placeholder="What needs to be done?" require />

                <button name="submit"></button>
            </form>
        </header>
        <section class="main">
            <input id="toggle-all" class="toggle-all" type="checkbox" />
            <label for="toggle-all"></label>
            <ul class="todo-list">
                <?php while ($query_result = mysqli_fetch_assoc($getData)) : ?>
                    <?php
                    $style = '';
                    if($query_result["status"] == '1')
                    {
                    $style = 'text-decoration: line-through';
                    } ?>
                    <?php
                    echo  '<li class="todo">
                        <div class="view">
                            <input class="toggle" type="checkbox" name="status" value="1" checked="unchecked"/>
                            
                            <label> ' . $query_result['title'] . ' </label>
                           <a href="index.php?id=' . $query_result["id"] . '"> <button  class="destroy" name="destroy"></button></a>
                            
                        </div>
                        <input class="edit" type="text" />
                    </li>';
                    ?>
                <?php endwhile; ?>
            </ul>
        </section>
        <footer class="footer">
            <span class="todo-count">
                <strong>hi</strong> ki
            </span>
            <ul class="filters">
                <li>
                    <a href="#/all">All</a>
                </li>
                <li>
                    <a href="#/active">Active</a>
                </li>
                <li>
                    <a href="#/completed">Completed</a>
                </li>
            </ul>
            <button class="clear-completed">
                Clear completed
            </button>
        </footer>
    </section>
    <footer class="info">
        <p>Double-click to edit a todo</p>
        <p>Written by <a href="http://evanyou.me">Evan You</a></p>
        <p>Part of <a href="http://todomvc.com">TodoMVC</a></p>
    </footer>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="assets/custom.js"></script>
</body>

</html>